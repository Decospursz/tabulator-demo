import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import Tabulator from 'tabulator-tables';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'DemoTabulator';
  people: IPerson[] = [];
  people2: IPerson[] = [];
  columnNames: any[] = [];
  myTable: Tabulator;
  myTable2: Tabulator;
  onUpdate(){
    alert('Updated Row!');
  }
  ngOnInit(){
    this.people = [
      { id: 1, firstName: 'John', lastName: 'Smith', state: 'Ohio', birthday: '', gender:'Male' },
      { id: 2, firstName: 'Jane', lastName: 'Doe', state: 'Iowa', birthday: '', gender:'Male' },
      { id: 3, firstName: 'Bill', lastName: 'Great', state: 'Hawaii', birthday: '', gender:'Female' },
      { id: 4, firstName: 'Ted', lastName: 'Adventure', state: 'Arizona', birthday: '', gender:'Bobrisky' }
    ];
    this.columnNames = [
      {title: 'Id', field: 'id'}, //frozen helps to freez the selected column from strolling along with other columns.
      {title: 'First Name', field: 'firstName', editor:'input'},
      {title: 'Last Name', field: 'lastName', editor:'input'},
      {title: 'State', field: 'state', editor:'input'},
      {title: 'Birth Day', field: 'birthday', align: 'center',
        sorter: 'date', editor: 'input'},
      { title: 'Gender', field:'gender', editor:"select", editorParams:{values:["Male", "Female", "Bobrisky"]}}
    ];

//#send: first table holding all the initial information from the array. 
    this.myTable = new Tabulator('#tabulator-div', {
      cellEdited(data){
        console.log(data._cell.row.data);
      },
      layout: 'fitColumns',
      groupBy: 'gender', //To group by a field, set this option to the name of the field.
      movableRows: true, // helps to activate movable row interaction within/without a table
      movableRowsConnectedTables:"#receiver_tabulator-div",  // describes the receiving table id
      // movableRowsReceiver: "add",
      groupStartOpen:false, //to set if the groups will be opened on init.
      // groupValues:[["Male", "Female", "Bobrisky"]],
      // groupUpdateOnCellEdit:true, //regroup a row when its groubBy cell is edited
      movableRowsSender: "delete", // deletes the moved row from the sender's table
      movableColumns: true  // helps to activate movable columns  interaction within a table
    });

//#receiver: second table receiving information from the sender table. 

    this.myTable2 = new Tabulator('#receiver_tabulator-div', {
      cellEdited(data){
        console.log(data._cell.row.data);
      },
      layout: 'fitColumns',
      movableRows: true,
      movableRowsConnectedTables:"#tabulator-div",  // describes the receiving table id
      movableRowsSender: "delete",
      groupBy: 'gender',
      movableColumns: true
    });

    this.myTable.setColumns(this.columnNames);
    this.myTable.setData(this.people);

    this.myTable2.setColumns(this.columnNames);
    this.myTable2.setData(this.people2);
  }
}
interface IPerson {
  id: number;
  firstName: string;
  lastName: string;
  state: string;
  birthday: string;
  gender:string;
}


